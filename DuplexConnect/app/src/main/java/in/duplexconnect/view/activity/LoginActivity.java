package in.duplexconnect.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import in.duplexconnect.R;

public class LoginActivity extends AppCompatActivity {

    private EditText userIdEditText;
    private EditText anotherUserIdEditText;

    private Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        this.userIdEditText = this.findViewById(R.id.editTexCurrUserId);
        this.anotherUserIdEditText = this.findViewById(R.id.editTextToUserId);

        this.loginButton = this.findViewById(R.id.buttonLogin);

        this.loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String currUserId = userIdEditText.getText().toString();
                String anotherUserId = anotherUserIdEditText.getText().toString();

                if (TextUtils.isEmpty(currUserId) || TextUtils.isEmpty(anotherUserId))
                    return;

                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                intent.putExtra("USER_ID", currUserId);
                intent.putExtra("ANOTHER_USER_ID", anotherUserId);
                startActivity(intent);
                finish();
            }
        });
    }
}
