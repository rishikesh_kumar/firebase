package in.duplexconnect.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import in.duplexconnect.MessageInfo;
import in.duplexconnect.R;
import in.duplexconnect.adapter.MessageAdapter;

public class MainActivity extends AppCompatActivity {

    private RecyclerView msgRecyclerView;
    private EditText inputEditText;
    private Button button;

    private String currUserId;
    private String anotherUserId;

    private DatabaseReference convoRef;

    private Query messageQuery;

    private List<MessageInfo> messageInfoList;
    private MessageAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_main);

        this.initView();
        this.initData();
        this.wireEventHandlers();
    }

    private void initView() {

        this.msgRecyclerView = this.findViewById(R.id.recyclerViewMessages);
        this.inputEditText = this.findViewById(R.id.edit_text_input);
        this.button = this.findViewById(R.id.button_send);
    }

    private void initData() {
        Intent intent = this.getIntent();
        this.currUserId = intent.getStringExtra("USER_ID");
        this.anotherUserId = intent.getStringExtra("ANOTHER_USER_ID");

        String convoId = getConversationIdentifier();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        this.convoRef = database.getReference("messages/" + convoId + "/");

        this.messageQuery = this.convoRef.orderByChild("{messageId}/timeStamp").limitToLast(50);
        this.messageInfoList = new ArrayList<>();
        this.adapter = new MessageAdapter(this.getLayoutInflater(), this.messageInfoList, isUserA());

        this.msgRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        this.msgRecyclerView.setAdapter(this.adapter);
    }

    private void wireEventHandlers() {

        this.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });

        this.messageQuery.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                messageInfoList.clear();

                Iterable<DataSnapshot> children = dataSnapshot.getChildren();

                for (DataSnapshot snapshot : children) {

                    if (snapshot.hasChild("messageType")) {
                        MessageInfo messageInfo = snapshot.getValue(MessageInfo.class);
                        messageInfoList.add(messageInfo);
                    }
                }

                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void sendMessage() {

        String messageValue = inputEditText.getText().toString();

        if (TextUtils.isEmpty(messageValue))
            return;

        long timeStamp = System.currentTimeMillis();

        MessageInfo messageInfo = new MessageInfo();
        messageInfo.setMessageType(isUserA() ? MessageInfo.MESSAGE_TYPE_USER_A_TO_USER_B : MessageInfo.MESSAGE_TYPE_USER_B_TO_USER_A);
        messageInfo.setTimeStamp(timeStamp);
        messageInfo.setValue(messageValue);

        String key = this.convoRef.push().getKey();
        this.convoRef.child(key).setValue(messageInfo);
        //this.convoRef.child("lastUpdatedTimeStamp").setValue(timeStamp);

        this.inputEditText.setText(null);
    }

    private String getConversationIdentifier() {

        String separator = "-";

        if (isUserA()) {
            return this.currUserId + separator + this.anotherUserId;
        } else {
            return this.anotherUserId + separator + this.currUserId;
        }
    }

    private boolean isUserA() {

        return true;
        //TODO
        //return this.currUserId.compareTo(this.anotherUserId) < 0;
    }
}