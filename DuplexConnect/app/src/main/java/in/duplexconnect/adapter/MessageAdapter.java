package in.duplexconnect.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import in.duplexconnect.MessageInfo;
import in.duplexconnect.R;
import in.duplexconnect.viewholder.MessageViewHolder;

public class MessageAdapter extends RecyclerView.Adapter<MessageViewHolder> {


    private LayoutInflater inflater;
    private List<MessageInfo> messageInfoList;
    private boolean isUserA;

    public MessageAdapter(LayoutInflater inflater, List<MessageInfo> messageInfoList, boolean isUserA) {
        this.inflater = inflater;
        this.messageInfoList = messageInfoList;
        this.isUserA = isUserA;
    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {

        View view = this.inflater.inflate(type == MessageViewHolder.TYPE_FROM ? R.layout.layout_msg_from : R.layout.layout_msg_to, viewGroup, false);
        MessageViewHolder viewHolder = new MessageViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MessageViewHolder messageViewHolder, int position) {

        MessageInfo messageInfo = this.messageInfoList.get(position);
        messageViewHolder.attachToView(messageInfo);
    }

    @Override
    public int getItemCount() {
        return messageInfoList.size();
    }

    @Override
    public int getItemViewType(int position) {

        MessageInfo messageInfo = this.messageInfoList.get(position);
        int messageType = messageInfo.getMessageType();

        if (this.isUserA) {

            if (messageType == MessageInfo.MESSAGE_TYPE_USER_A_TO_USER_B) {
                return MessageViewHolder.TYPE_FROM;
            } else {
                return MessageViewHolder.TYPE_TO;
            }

        } else {

            if (messageType == MessageInfo.MESSAGE_TYPE_USER_A_TO_USER_B) {
                return MessageViewHolder.TYPE_TO;
            } else {
                return MessageViewHolder.TYPE_FROM;
            }
        }
    }
}
