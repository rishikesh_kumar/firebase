package in.duplexconnect.viewholder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.duplexconnect.MessageInfo;
import in.duplexconnect.R;

public class MessageViewHolder extends RecyclerView.ViewHolder {

    public static final int TYPE_FROM = 1;
    public static final int TYPE_TO = 2;

    private TextView textView;

    public MessageViewHolder(@NonNull View itemView) {
        super(itemView);
        this.textView = itemView.findViewById(R.id.textViewMessage);
    }

    public void attachToView(MessageInfo messageInfo) {
        textView.setText(messageInfo.getValue());
    }
}
