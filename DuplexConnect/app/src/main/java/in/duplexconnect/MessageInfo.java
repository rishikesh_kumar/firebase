package in.duplexconnect;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.Date;

@IgnoreExtraProperties
public class MessageInfo {

    public static final int MESSAGE_TYPE_USER_A_TO_USER_B = 1;
    public static final int MESSAGE_TYPE_USER_B_TO_USER_A = 2;

    private int messageType;
    private long timeStamp;
    private String value;

    public int getMessageType() {
        return messageType;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
