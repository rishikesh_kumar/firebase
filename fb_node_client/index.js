const firebase = require('firebase');
const dateFormat = require('dateformat');


firebase.initializeApp({
    databaseURL: 'https://duplexconnect.firebaseio.com/',
    serviceAccount: 'duplex_connect_service_account.json',
});

const PATH_MESSAGES = '/messages/';

firebase
    .database()
    .ref(PATH_MESSAGES)
    .orderByChild('/{convoId}/{messageId}/timeStamp')    
    .on('child_changed', function (convoSnap) {        

        var lastUpdatedMessageRef = firebase
            .database()
            .ref(PATH_MESSAGES + convoSnap.key)
            .orderByChild('timeStamp')
            .limitToLast(1);

        lastUpdatedMessageRef.once('value', onMessageReceiveCallback);
    })

const onMessageReceiveCallback = async function (filteredConvoSnap) {    

    const convoId = filteredConvoSnap.key;
    const filteredConvo = filteredConvoSnap.val();

    for (const msgId in filteredConvo) {        

        const msg = filteredConvo[msgId];

        if (msg.messageType == 1) {            

            console.log('New Message Received ' + convoId + '(convoId) ' + msgId + '(msgId) - ' + msg.value);
            var response = await apiRequest();

            var replyMsg = {
                'value': response,
                'messageType': 2,
                'timeStamp': new Date().getTime()
            };

            console.log(replyMsg);

            firebase.database().ref(PATH_MESSAGES + convoId).push(replyMsg);
        }
    }
}

const apiRequest = async function () {

    const url = 'https://travel.paytm.com/bus/v2/search?client=web';
    const date = dateFormat(new Date(), 'yyyy-mm-dd');

    return fetch(url,
        {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                "date": date,
                "dest_display_name": "Chennai",
                "src_display_name": "Bengaluru",
                "is_last_minute_booking": true,
                "sold": true,
                "departed": true
            })
        })
        .then(response => response.json())
        .then(apiResponse => {

            var travelServiceNames = new Set();
            var body = apiResponse['body'];

            body.forEach(function (busInfo) {
                travelServiceNames.add(busInfo['travelsName']);
            });

            return travelServiceNames.size + ' - unique travel services found';
        })
        .catch(error => console.error('Error:', error));
}